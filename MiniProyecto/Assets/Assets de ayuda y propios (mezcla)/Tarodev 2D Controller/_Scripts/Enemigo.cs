using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    //Primera parte que va con las vidas del personaje
   private void OnCollisionEnter2D(Collision2D other)
   {
        if(other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerCombat>().TomarDaño(1, other.GetContact(0).normal);
            GameManager.Instance.PerderVida();
        }
   }

    //Segunda parte que va con las vidas de los enemigos

    [SerializeField] private float vidaEnemigo;

    public void TomarDañoEnemigo(float dañoEnemigo)
    {
        vidaEnemigo -= dañoEnemigo;

        if (vidaEnemigo <= 0)
        {
            Muerte();
        }
    }

    private void Muerte()
    {
        Destroy(this.gameObject);
    } 

}
