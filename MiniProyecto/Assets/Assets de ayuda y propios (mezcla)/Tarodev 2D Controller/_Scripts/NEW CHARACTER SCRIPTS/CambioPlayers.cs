using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioPlayers : MonoBehaviour
{
// Publicas
    public GameObject NEW_Player_Ligth;
    public GameObject NEW_Player_Dark;
//Privadas
    private bool Change = false;
    private SpriteRenderer skinLigth;
    private SpriteRenderer skinDark;
    private CapsuleCollider2D colliderLigth;
    private CapsuleCollider2D colliderDark;

    void Awake()
    {
        skinLigth=NEW_Player_Ligth.GetComponent<SpriteRenderer>();
        skinDark=NEW_Player_Dark.GetComponent<SpriteRenderer>();
        colliderLigth=NEW_Player_Ligth.GetComponent<CapsuleCollider2D>();
        colliderDark=NEW_Player_Dark.GetComponent<CapsuleCollider2D>();
    }

    void Start()
    {
        skinDark.enabled = false;
        colliderDark.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire4") && Change == false)
        {
            Change = true;
            skinLigth.enabled = false;
            colliderLigth.enabled = false;
            skinDark.enabled = true;
            colliderDark.enabled = true;
        }

        else  if(Input.GetButtonDown("Fire4") && Change == true)
        {
            Change = false;
            skinLigth.enabled = true;
            colliderLigth.enabled = true;
            skinDark.enabled = false;
            colliderDark.enabled = false;
        }
        
    }
}
