using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWPlayerCombat : MonoBehaviour
{
    [SerializeField] private float vida;
    private NEWControlJugador controlJugador;
    [SerializeField] private float tiempoPerdidaControl;

    private Animator animator;

    private void Start()
    {
        controlJugador = GetComponent<NEWControlJugador>();
        animator = GetComponent<Animator>();
    }


    public void TomarDaño(float daño)
    {
        vida-= daño;
    }

    public void TomarDaño(float daño, Vector2 posicion)
    {
        vida-= daño;
        animator.SetTrigger("Player Damaged");
        StartCoroutine(PerderControl());
        //DesactivarColisión de Enemigos.
        StartCoroutine(DesactivarColisión());
        controlJugador.Rebote(posicion);
    }

    private IEnumerator DesactivarColisión()
    {
        Physics2D.IgnoreLayerCollision(6, 3, true);
        yield return new WaitForSeconds(tiempoPerdidaControl);
        Physics2D.IgnoreLayerCollision(6, 3, false);
    }

    private IEnumerator PerderControl()
    {
        controlJugador.CanMove = false;
        yield return new WaitForSeconds(tiempoPerdidaControl);
        controlJugador.CanMove = true;
    }
}