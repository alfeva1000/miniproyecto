using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NEWCambiarColor : MonoBehaviour
{
    //Publico
    public GameObject enemigoLigth;
    public GameObject enemigoDark;
    private CircleCollider2D[] colliderconderechos;
    private CircleCollider2D[] collidersinderechos;
    //Privado
    private SpriteRenderer skin;
    //private Color colors;
    private bool Change = false;
    // Start is called before the first frame update
    void Start()
    {
        //colors = new Color
        skin = GetComponent<SpriteRenderer>();
        colliderconderechos = enemigoLigth.GetComponentsInChildren<CircleCollider2D>();
        collidersinderechos = enemigoDark.GetComponentsInChildren<CircleCollider2D>();

        foreach (var items in collidersinderechos)
        {
            items.enabled = true;
        }
        foreach (var items in colliderconderechos)
        {
            items.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire4") && Change == false)
        {
            Change = true;
            skin.color = Color.red;
            foreach (var items in collidersinderechos)
            {
                items.enabled = false;
            }
            foreach (var items in colliderconderechos)
            {
                items.enabled = true;
            }
        }

        else if(Input.GetButtonDown("Fire4") && Change == true)
        {
            Change = false;
            skin.color = Color.white;
            foreach (var items in collidersinderechos)
            {
                items.enabled = true;
            }
            foreach (var items in colliderconderechos)
            {
                items.enabled = false;
            }
        }
    }


}
