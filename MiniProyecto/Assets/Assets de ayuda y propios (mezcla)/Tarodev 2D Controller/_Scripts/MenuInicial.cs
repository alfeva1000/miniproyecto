using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuInicial : MonoBehaviour
{

    public void Level1()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Nivel 1-1");
    }

     public void Level2()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Nivel 1-2");
    }

     public void Level3()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Nivel 1-3");
    }

    public void Exit()
    {
        Debug.Log("Salir...");
        Application.Quit();
    }
 
}
