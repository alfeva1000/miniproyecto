using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateMele : MonoBehaviour
{
    [Header("Melee")]
    [SerializeField] private Transform controladorGolpe;
    [SerializeField] private float radioGolpe;
    [SerializeField] private float dañoGolpe;
    [SerializeField] private float tiempoEntreAtaquesMelee;
    [SerializeField] private float tiempoSiguienteAtaqueMelee;

    [Header("Range")]
    [SerializeField] private Transform controladorDisparo;
    [SerializeField] private GameObject bala;
    [SerializeField] private float tiempoEntreAtaquesRange;
    [SerializeField] private float tiempoSiguienteAtaqueRange;

    private Animator animator;
    

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

   private void Update()
   {
        if (tiempoSiguienteAtaqueMelee > 0)
        {
            tiempoSiguienteAtaqueMelee -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1") && tiempoSiguienteAtaqueMelee <= 0)
        {
            Golpe();
            tiempoSiguienteAtaqueMelee = tiempoEntreAtaquesMelee;
        }


        if (tiempoSiguienteAtaqueRange > 0)
        {
            tiempoSiguienteAtaqueRange -= Time.deltaTime;
        }
        if (Input.GetButtonDown("Fire2") && tiempoSiguienteAtaqueRange <= 0)
        {
            Disparar();
            tiempoSiguienteAtaqueRange = tiempoEntreAtaquesRange;
        }
   }

   private void Golpe()
   {
        animator.SetTrigger("Golpe");
        
        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorGolpe.position, radioGolpe);

        foreach (Collider2D colisionador in objetos)
        {
            if (colisionador.CompareTag("Enemies"))
            {
                colisionador.transform.GetComponent<NEWEnemigo>().TomarDañoEnemigo(dañoGolpe);
            }
        }
   }

   private void Disparar()
   {
        animator.SetTrigger("Disparo");
        Instantiate(bala, controladorDisparo.position, controladorDisparo.rotation);
   }



   private void OnDrawGizmos()
   {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(controladorGolpe.position, radioGolpe);
   }

}
