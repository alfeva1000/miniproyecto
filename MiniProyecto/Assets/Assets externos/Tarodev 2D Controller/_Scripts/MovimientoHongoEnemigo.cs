using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoHongoEnemigo : MonoBehaviour
{
    public LayerMask mask;
    [SerializeField] private float velocidad;

    [SerializeField] private Transform controladorSuelo;

    [SerializeField] private Transform controladorPared;

    [SerializeField] private float distancia;

    [SerializeField] private bool MovimientoIzquierda;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        RaycastHit2D informacionSuelo = Physics2D.Raycast(controladorSuelo.position, Vector2.down, distancia);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);

        if (informacionSuelo == false)
        {
            Girar();
        }

        
    }

    private void Girar()
    {
        MovimientoIzquierda = !MovimientoIzquierda;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180,0);
        velocidad *= -1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(controladorSuelo.transform.position, controladorSuelo.transform.position + Vector3.down * distancia);
         Gizmos.color = Color.red;
        Gizmos.DrawLine(controladorPared.transform.position, controladorPared.transform.position + Vector3.down * distancia);
    }
}
