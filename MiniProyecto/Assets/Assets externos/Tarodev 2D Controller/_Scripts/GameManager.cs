using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class GameManager : MonoBehaviour
{

    public static GameManager Instance { get; private set; }
    public HUD hud;
    public int PuntosTotales { get { return puntosTotales; } }
    private int puntosTotales;

    private int vidas = 4;

    void Awake() 
    {
        if(Instance == null)
        {
            Instance = this;
        } else
        {
            Debug.Log("Tienes mas de un GameManager en esta escena");
        }
    }

    public void SumarPuntos(int puntosASumar)
    {
        puntosTotales += puntosASumar;
        hud.ActualizarPuntos(PuntosTotales);
    }

    public void PerderVida()
    {
        vidas -= 1;

        if(vidas == 0)
        {
            //Se reinicia el nivel.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
        }
        hud.DesactivarVida(vidas);
    }

    public bool RecuperarVida()
    {
        if (vidas == 4)
        {
            return false;
        }

        hud.ActivarVida(vidas);
        vidas += 1;
        return true;
    }

    public void MuerteAbismo()
    {
        vidas-= 4;

        if(vidas <= 0)
        {
            //Se reinicia el nivel.
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
        }
        //hud.DesactivarVida(vidas);
    }
}
